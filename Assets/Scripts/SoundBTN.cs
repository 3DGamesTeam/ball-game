﻿using UnityEngine;
using UnityEngine.UI;

public class SoundBTN : MonoBehaviour
{
	// sound == 0 -> sound is on
	// sound == 1 -> sound is off
    bool sound;
    public Texture soundOn;
    public Texture soundOff;

    void Awake()
    {
		// get if the last sound state was 0 = on or 1 = off
        sound = PlayerPrefs.GetInt("sound") == 1;
        changeImage();
    }

	void Start()
	{
		SoundController.soundInstance.CheckMusic();
		SoundController.soundInstance.ControlMute(sound);
	}

    public void StartOrStop()
    {
        sound = !sound;
		SoundController.soundInstance.ControlMute(sound);
        changeImage();

        PlayerPrefs.SetInt("sound", (sound ? 1 : 0));
    }

    void changeImage()
    {

        if (sound == false)
        {
			this.GetComponent<RawImage>().texture = soundOn;
        }
        else
        {
			this.GetComponent<RawImage>().texture = soundOff;
        }
    }
}
