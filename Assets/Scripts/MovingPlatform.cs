﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour
{
	public int movementSpeed = 3;

	void Update()
	{
		this.transform.position = new Vector2(this.transform.position.x + (movementSpeed * Time.deltaTime), this.transform.position.y);
	}

	void OnCollisionEnter2D(Collision2D target)
	{
		if (target.gameObject.tag == "Wall")
		{
			movementSpeed = -movementSpeed;
		}
	}
}
