﻿using UnityEngine;
using UnityEngine.UI;

public class SelectSkinManager : MonoBehaviour
{
    public int activeSkinIndex = 0;
	public SpriteRenderer playerRenderer;
    public GameObject useSkinButton;
    public Text message;

    void Awake()
    {
        //activeSkinIndex = GameController.gController.lastUsedSkinIndex;

		playerRenderer = GameObject.Find("Player2D").GetComponent<SpriteRenderer>();
		playerRenderer.sprite = SkinsController.skController.allSkinTextures[activeSkinIndex];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[activeSkinIndex];
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.gController.skinsBought[activeSkinIndex] == GameController.gController.lastUsedSkinIndex)
        {
            message.text = "Current skin";
            useSkinButton.SetActive(false);
        }
        else
        {
            //Debug.Log(GameController.gController.skinsBought[activeSkinIndex]);
            message.text = string.Empty;
            useSkinButton.SetActive(true);
        }
    }

    public void LoadNextSkin()
    {
        activeSkinIndex++;
        if (activeSkinIndex >= GameController.gController.skinsBought.Count)
        {
            activeSkinIndex = 0;
        }

		playerRenderer.sprite = SkinsController.skController.allSkinTextures[GameController.gController.skinsBought[activeSkinIndex]];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[GameController.gController.skinsBought[activeSkinIndex]];
    }

    public void LoadPreviousSkin()
    {
        activeSkinIndex--;
        if (activeSkinIndex < 0)
        {
            activeSkinIndex = GameController.gController.skinsBought.Count - 1;
        }

		playerRenderer.sprite = SkinsController.skController.allSkinTextures[GameController.gController.skinsBought[activeSkinIndex]];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[GameController.gController.skinsBought[activeSkinIndex]];
    }

    public void UseSkinButtonPress()
    {
        GameController.gController.lastUsedSkinIndex = GameController.gController.skinsBought[activeSkinIndex];
    }
}
