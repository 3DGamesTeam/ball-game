﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class ActionBTNS : MonoBehaviour
{
    private int sceneIndex;
    private GameObject[] sliderHolders;
    public int activeIndex;
    public bool activePowerSpeed;

	private GameObject infMsg;
	private Text infTxt;

    void Awake()
    {
        activeIndex = PlayerPrefs.GetInt("activeLevelSlider");
    }

    void Start()
    {
        // Get current scene name
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (sceneIndex == 1)
        {
            // NOT SURE IF using System.Linq WILL WORK ON ANDROID. TO BE DOUBLE-CHECKED AND TESTED
            sliderHolders = GameObject.FindGameObjectsWithTag("MenuSlider").OrderBy(obj => obj.GetComponent<RectTransform>().GetSiblingIndex()).ToArray();

            for (int i = 0; i < sliderHolders.Length; i++)
            {
                if (i == activeIndex)
                {
                    sliderHolders[i].GetComponent<RectTransform>().localScale = Vector3.one;
                }
                else
                {
                    sliderHolders[i].GetComponent<RectTransform>().localScale = Vector3.zero;
                }
            }
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(sceneIndex + 1);
    }

    public void ReloadScene()
    {
		UnpauseGameIfPaused();
        SceneManager.LoadScene(sceneIndex);
    }

    public void LevelSelect()
    {
        // get the name of the game object that is clicked to call this method
        SceneManager.LoadScene(EventSystem.current.currentSelectedGameObject.name);
        SaveMenuIndex(activeIndex);
    }

    public void NextSlide()
    {
        int prevIndexInArray = activeIndex;
        activeIndex++;
        if (activeIndex >= sliderHolders.Length)
        {
            activeIndex = sliderHolders.Length - 1;
            prevIndexInArray = activeIndex - 1;
        }

        ShowCorrectLevelSlider(activeIndex, prevIndexInArray);
    }

    public void PrevSlide()
    {
        int nextIndexInArray = activeIndex;
        activeIndex--;
        if (activeIndex < 0)
        {
            activeIndex = 0;
            nextIndexInArray = 1;
        }

        ShowCorrectLevelSlider(activeIndex, nextIndexInArray);
    }

    private void ShowCorrectLevelSlider(int active, int inactive)
    {
        sliderHolders[active].GetComponent<RectTransform>().localScale = Vector3.one;
        sliderHolders[inactive].GetComponent<RectTransform>().localScale = Vector3.zero;
    }

    public void unlockLevel()
    {
        GameObject unlockMenu = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().unlockLevelMenu;
        unlockMenu.SetActive(true);
		Text unlockMenuText = unlockMenu.GetComponentInChildren<Text>();
		unlockMenuText.text = string.Format("Unlocking the next level costs {0} tokens. Are you sure?", GameLevelTimes.UnlockLevelWithTokensPrice);
    }

    public void closeUnlockMenu()
    {
        GameObject unlockMenu = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().unlockLevelMenu;
        unlockMenu.SetActive(false);
    }

    public void unlockNextLevelWithTokens()
    {
        int currentLevelIndex = SceneManager.GetActiveScene().buildIndex;
		string nextLevelName = "Level" + currentLevelIndex;

		if ((GameController.gController.tokens - GameLevelTimes.UnlockLevelWithTokensPrice) >= 0)
        {
			Debug.Log(string.Format("You have unlocked the next level - {0} for {1} tokens!", nextLevelName, GameLevelTimes.UnlockLevelWithTokensPrice));
			GameController.gController.playerData.Add(new LevelData(nextLevelName, 0, 0.0f));
			GameController.gController.tokens -= GameLevelTimes.UnlockLevelWithTokensPrice;
            GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().unlockNextLevel(nextLevelName);
			GameController.gController.Save();
			closeUnlockMenu();
        }
        else
        {
			infMsg = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().infoMessage;
			infTxt = infMsg.GetComponentInChildren<Text>();
			infTxt.text = "You do not have enough tokens! You can watch a video ad to get tokens!";
			infMsg.SetActive(true);
        }
    }

    public void SpeedUP()
    {
        activePowerSpeed = !activePowerSpeed;
    }


    public void LoadLevelMenu()
    {
		UnpauseGameIfPaused();
        SceneManager.LoadScene(1);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadShop()
    {
        SceneManager.LoadScene("SkinShop");
    }

    public void SelectSkin()
    {
        SceneManager.LoadScene("SelectSkin");
    }

    public void Quit()
    {
        GameController.gController.Save();
        Application.Quit();
    }

    private void SaveMenuIndex(int index)
    {
        PlayerPrefs.SetInt("activeLevelSlider", index);
    }

	private void UnpauseGameIfPaused()
	{
		if (GameController.gController.gameIsPaused)
		{
			Time.timeScale = 1;
			GameController.gController.gameIsPaused = false;

		}
	}
}
