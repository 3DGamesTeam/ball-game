﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public static GameController gController;

    public int tokens;
	public List<LevelData> playerData;
    public int speedPowerUps;
    public int ghostPowerUps;
    public List<int> skinsBought;
    public int lastUsedSkinIndex;
	public bool gameIsPaused = false;

    void Awake()
    {
        if (gController == null)
        {
            gController = this;
            gController.Load();
            DontDestroyOnLoad(gameObject);
        }
        else if (gController != this)
        {
            Destroy(gameObject);
        }
    }
		
    /// <summary>
    /// Save the game data. To be called before quitting the application 
	/// and on other occasions when you need to save some game progress.
    /// </summary>
    public void Save()
    {
        DataDTO saveDataDTO = new DataDTO();
		saveDataDTO.tns = GameController.gController.tokens;
		saveDataDTO.gpu = GameController.gController.ghostPowerUps;
		saveDataDTO.spu = GameController.gController.speedPowerUps;
		saveDataDTO.ls = GameController.gController.playerData;
		saveDataDTO.skb = GameController.gController.skinsBought;
		saveDataDTO.lusi = GameController.gController.lastUsedSkinIndex;

        string saveFile = Application.persistentDataPath + "/pinfd.file";
        Debug.Log(saveFile);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveFile);

        try
        {
            bf.Serialize(file, saveDataDTO);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        finally
        {
            file.Close();   
        }
    }
		
    /// <summary>
    /// Loads the saved game data. To be called immediately, when the game is started.
    /// </summary>
    public void Load()
    {
        string loadFile = Application.persistentDataPath + "/pinfd.file";

        if (File.Exists(loadFile))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(loadFile, FileMode.Open);

            try
            {
                DataDTO loadDataDTO = (DataDTO)bf.Deserialize(file);
				tokens = loadDataDTO.tns;
				ghostPowerUps = loadDataDTO.gpu;
				speedPowerUps = loadDataDTO.spu;
				playerData = loadDataDTO.ls;
				skinsBought = loadDataDTO.skb;
				lastUsedSkinIndex = loadDataDTO.lusi;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
            finally
            {
                file.Close();
            }

            //THIS ONE TO BE REVISED AGAIN. POTENTIAL NULL EXCEPTION COULD BE THROWN.
            //Check all skins that you have bought and can use
            //and update the default values inside Skins.skinShop
            //if (SkinsController.skController != null)
            //{
            for (int i = 0; i < skinsBought.Count; i++)
            {
                if (SkinsController.skController.skinShopData.ContainsKey(skinsBought[i]))
                {
                    SkinsController.skController.skinShopData[skinsBought[i]].IsBought = true;
                }
            }
            //}
        }
        else
        {
            LoadDefaultPlayerData();
        }
    }

    // If the saved file does not exits, load some default zero data.
    private void LoadDefaultPlayerData()
    {
        tokens = 12500;
        speedPowerUps = 90;
        ghostPowerUps = 90;
		playerData = new List<LevelData>();
		playerData.Add(new LevelData("Level1", 0, 0f));
        skinsBought = new List<int>() { 0 };
        lastUsedSkinIndex = 0;
    }
}

[Serializable]
class DataDTO
{
	//tokens
    public int tns;
	//levels
    public List<LevelData> ls;
    //speed power ups
	public int spu;
	//ghost power up
    public int gpu;
	//skins bought
    public List<int> skb;
	//last used skin index
    public int lusi;
}

[Serializable]
public class LevelData
{
    private string name;
    private int stars;
    private float bestTime;

    public LevelData(string name, int stars, float bestTime)
    {
        this.Name = name;
        this.Stars = stars;
        this.BestTime = bestTime;
    }

    public string Name
    {
        get { return this.name; }
        set
        {
            this.name = value;
        }
    }

    public int Stars
    {
        get { return this.stars; }
        set
        {
            if (value <= 0)
            {
                this.stars = 0;
            }
            else
            {
                this.stars = value;
            }
        }
    }

    public float BestTime
    {
        get { return this.bestTime; }
        set
        {
            if (value <= 0f)
            {
                this.bestTime = 0f;
            }
            else
            {
                this.bestTime = value;
            }
        }
    }
}