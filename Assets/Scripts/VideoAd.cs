﻿    using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class VideoAd : MonoBehaviour
{
	public GameObject afterAdPanel;
	//public GameObject adButton;
	//public int rewardAmount;
	public TokenSystem token;

	void Start()
	{
		//if(adButton == null)
		//{
		//    return;
		//}
		if (afterAdPanel == null)
		{
			afterAdPanel = GameObject.FindGameObjectWithTag("AdPanel");
		}
		if(afterAdPanel != null)
        {
			afterAdPanel.SetActive(false);
		}
            
        token = GetComponent<TokenSystem>();
        //if (!Advertisement.IsReady("rewardedVideo"))
        //{
        //    adButton.SetActive(false);
        //}
        //if (Advertisement.IsReady("rewardedVideo"))
        //{
        //    adButton.SetActive(true);
        //}
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
		case ShowResult.Finished:
			// Giving The Player Reward(Tokens)
			Debug.Log("The ad was successfully shown.");
			token.addTokens(GameLevelTimes.WatchVideoAdReward);
			popPanel();
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
        }
    }

    public void popPanel()
    {
        afterAdPanel.SetActive(true);
    }

    public void closePanel()
    {
        afterAdPanel.SetActive(false);
    }
}
