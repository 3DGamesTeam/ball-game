﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkinsController : MonoBehaviour
{
    public static SkinsController skController;

	public Sprite[] allSkinTextures;
    public Dictionary<int, SkinInfo> skinShopData;

    void Awake()
    {
        if (skController == null)
        {
            skController = this;
            skController.LoadSkinData();
            DontDestroyOnLoad(gameObject);
        }
        else if (skController != this)
        {
            Destroy(gameObject);
        }
    }

    public void LoadSkinData()
    {
		allSkinTextures = Resources.LoadAll<Sprite>("Skins");
        skinShopData = new Dictionary<int, SkinInfo>()
        {
            { 0, new SkinInfo(250, true) },
            { 1, new SkinInfo(250, false) },
            { 2, new SkinInfo(250, false) },
            { 3, new SkinInfo(250, false) },
            { 4, new SkinInfo(250, false) },
            { 5, new SkinInfo(250, false) },
            { 6, new SkinInfo(250, false) },
            { 7, new SkinInfo(450, false) },
            { 8, new SkinInfo(450, false) },
            { 9, new SkinInfo(550, false) }
        };
    }
}

public class SkinInfo
{
    public SkinInfo(int price, bool isBought)
    {
        this.Price = price;
        this.IsBought = isBought;
    }

    public int Price { get; set; }
    public bool IsBought { get; set; }
}