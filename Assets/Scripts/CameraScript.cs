﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	private GameObject player;
    private GameObject timer;
	private bool escActive = false;
    private GameObject pauseMenu;
    public bool isFocused;
	private FinishLine finishLine;
    public float orthographicSize;
    public float aspect;
    


    void Start () {
		player = GameObject.Find ("Player2D") as GameObject;
        timer = GameObject.Find("Timer");
        pauseMenu = GameObject.Find("PauseMenu");
        pauseMenu.SetActive(false);
        isFocused = true;
        finishLine = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        // Transform camera positon Y
        if (isFocused)
        {
            transform.position = new Vector3(this.transform.position.x, player.transform.position.y, this.transform.position.z);
        }
        
		// If player press escape button game will be paused 
        escBtn();
	}

    void escBtn()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
			PauseButtonPress();
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Finish")
        {
            isFocused = false;
        }
    }

	public void PauseButtonPress()
	{
		timer.GetComponent<Timer>().enabled = !timer.GetComponent<Timer>().enabled;

		if (escActive == false)
		{
			player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
			escActive = true;
			pauseMenu.SetActive(true);
			finishLine.ghostButton.enabled = false;
			finishLine.speedButton.enabled = false;
			Time.timeScale = 0;
			GameController.gController.gameIsPaused = true;
		}
		else
		{
			player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
			escActive = false;
			pauseMenu.SetActive(false);
			finishLine.ghostButton.enabled = true;
			finishLine.speedButton.enabled = true;
			Time.timeScale = 1;
			GameController.gController.gameIsPaused = false;
		}
	}
}
