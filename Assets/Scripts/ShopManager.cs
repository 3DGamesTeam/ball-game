﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ShopManager : MonoBehaviour
{
    public GameObject shopMenuBtns;
    public GameObject powerUpCanvas;
    public GameObject skinShopCanvas;
	public GameObject confirmationMessage;
	public GameObject errMessage;

	public Text confirmationMessageText;
	public Text errMessageText;
    public Text ghostCost;
    public Text ghostAmount;
    public Text speedCost;
    public Text speedAmount;

    public Text skinCost;
    public GameObject unlockSkinButton;
    public GameObject useSkinButton;
    public GameObject ball;

    // The below 4 should be private.
    // I left them public for testing purposes.
    public int currentIndex = 0;
	public SpriteRenderer playerRenderer;
	public string buttonTrigger;
	public string yesNoButtonClicked;
	public GameObject btnPressed;

    void Awake()
    {
		playerRenderer = GameObject.Find("Player2D").GetComponent<SpriteRenderer>();
		playerRenderer.sprite = SkinsController.skController.allSkinTextures[currentIndex];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[currentIndex];

        ball.SetActive(false);
        powerUpCanvas.SetActive(false);
        skinShopCanvas.SetActive(false);
		confirmationMessage.SetActive(false);
		errMessage.SetActive(false);
    }

    void Update()
    {
        if (powerUpCanvas.activeSelf)
        {
			ghostCost.text = string.Format("Cost: {0}", GameLevelTimes.GhostPowerUpPrice);
			speedCost.text = string.Format("Cost: {0}", GameLevelTimes.SpeedPowerUpPrice);
            ghostAmount.text = string.Format("Current: {0}", GameController.gController.ghostPowerUps);
            speedAmount.text = string.Format("Current: {0}", GameController.gController.speedPowerUps);
        }

        if (skinShopCanvas.activeSelf)
        {
            if (!SkinsController.skController.skinShopData[currentIndex].IsBought)
            {
                skinCost.text = string.Format("Cost: {0}", SkinsController.skController.skinShopData[currentIndex].Price);
                ActivateCorrectSkinButton(0);
            }
            else
            {
                skinCost.text = string.Empty;
                if (GameController.gController.lastUsedSkinIndex == currentIndex)
                {
                    skinCost.text = "Current skin";
                    ActivateCorrectSkinButton(-1);
                }
                else
                {
                    ActivateCorrectSkinButton(1);
                }
            }
        }

		if (confirmationMessage.activeSelf)
		{
			if (EventSystem.current.currentSelectedGameObject != null) {
				yesNoButtonClicked = EventSystem.current.currentSelectedGameObject.name;
			}
			else
			{
				CloseConfirmationMessage();
			}

			if (yesNoButtonClicked.Equals("Yes"))
			{
				switch (buttonTrigger)
				{
				case "Ghost":
					Debug.Log("Yes.....Buy Ghost power up!!!");
					BuyGhostPowerUp();
					break;
				case "SpeedUP":
					Debug.Log("Yes.....Buy Speed power up!!!");
					BuySpeedPowerUp();
					break;
				case "BuySkin":
					Debug.Log("Yes.....Buy new skin!!!");
					BuySkin();
					break;
				}
				CloseConfirmationMessage();
			}
			else if (yesNoButtonClicked.Equals("No")) 
			{
				CloseConfirmationMessage();
				Debug.Log("No...was clicked!!!");
			}
		}

		if (errMessage.activeSelf)
		{
			btnPressed = EventSystem.current.currentSelectedGameObject;
			if (btnPressed == null || btnPressed.name.Equals("AdButton") || btnPressed.name.Equals("No"))
			{
				CloseErrorMessage();
			}
		}
    }

    public void ActivateSkinShopScreen()
    {
        shopMenuBtns.SetActive(false);
        skinShopCanvas.SetActive(true);
        ball.SetActive(true);
    }

    public void ActivatePowerUpShopScreen()
    {
        shopMenuBtns.SetActive(false);
        powerUpCanvas.SetActive(true);
    }

    public void ActivateShopInitialScreen()
    {
        skinShopCanvas.SetActive(false);
        ball.SetActive(false);
        powerUpCanvas.SetActive(false);
        shopMenuBtns.SetActive(true);
		CloseConfirmationMessage();
		CloseErrorMessage();
    }

	private void ShowConfirmationMessage(int itemPrice)
	{
		buttonTrigger = EventSystem.current.currentSelectedGameObject.name;
		confirmationMessage.SetActive(true);
		confirmationMessageText.text = string.Format("Spend {0} tokens for this. Are you sure?", itemPrice);
	}

	private void CloseConfirmationMessage()
	{
		buttonTrigger = string.Empty;
		confirmationMessageText.text = string.Empty;
		confirmationMessage.SetActive (false);
	}

	private void ShowErrorMessage(int option)
	{
		if (option == 0)
		{
			errMessageText.text = "You do not have enough tokens! You can watch a video ad to get tokens!";
		}
		else if (option == 1)
		{
			errMessageText.text = "You have reached the maximum of 99 available power ups!";
		}

		errMessage.SetActive(true);
	}

	private void CloseErrorMessage()
	{
		errMessageText.text = string.Empty;
		errMessage.SetActive(false);
	}

	public void GhostPowerUpConfirmationMessage()
	{
		ShowConfirmationMessage(GameLevelTimes.GhostPowerUpPrice);
	}

	private void BuyGhostPowerUp()
    {
		if (GameController.gController.tokens >= GameLevelTimes.GhostPowerUpPrice && GameController.gController.ghostPowerUps < 99)
        {
            GameController.gController.ghostPowerUps++;
			GameController.gController.tokens -= GameLevelTimes.GhostPowerUpPrice;
        }
        else
        {
			if (GameController.gController.ghostPowerUps >= 99)
			{
				ShowErrorMessage(1);
			}
			else
			{
				ShowErrorMessage(0);
			}
        }
    }

	public void SpeedPowerUpConfirmationMessage()
	{
		ShowConfirmationMessage(GameLevelTimes.SpeedPowerUpPrice);
	}

	private void BuySpeedPowerUp()
    {
		if (GameController.gController.tokens >= GameLevelTimes.SpeedPowerUpPrice && GameController.gController.speedPowerUps < 99)
        {
            GameController.gController.speedPowerUps++;
			GameController.gController.tokens -= GameLevelTimes.SpeedPowerUpPrice;
        }
        else
        {
			if (GameController.gController.speedPowerUps >= 99)
			{
				ShowErrorMessage(1);
			}
			else
			{
				ShowErrorMessage(0);
			}
        }
    }

	public void BuySkinConfirmationMessage()
	{
		ShowConfirmationMessage(SkinsController.skController.skinShopData[currentIndex].Price);
	}

	private void BuySkin()
    {
        int skinPrice = SkinsController.skController.skinShopData[currentIndex].Price;

        if (GameController.gController.tokens >= skinPrice && !SkinsController.skController.skinShopData[currentIndex].IsBought)
        {
            SkinsController.skController.skinShopData[currentIndex].IsBought = true;
            GameController.gController.tokens -= skinPrice;
            GameController.gController.skinsBought.Add(currentIndex);
        }
        else
        {
			ShowErrorMessage(0);
        }
    }

    public void UseSkin()
    {
        if (SkinsController.skController.skinShopData[currentIndex].IsBought)
        {
            GameController.gController.lastUsedSkinIndex = currentIndex;
        }
    }

    public void LoadNextSkin()
    {
		if (confirmationMessage.activeSelf || errMessage.activeSelf)
		{
			CloseConfirmationMessage();
			CloseErrorMessage();
		}
		
        currentIndex++;
        if (currentIndex >= SkinsController.skController.allSkinTextures.Length)
        {
            currentIndex = 0;
        }

		playerRenderer.sprite = SkinsController.skController.allSkinTextures[currentIndex];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[currentIndex];
    }

    public void LoadPreviousSkin()
    {
		if (confirmationMessage.activeSelf || errMessage.activeSelf)
		{
			CloseConfirmationMessage();
			CloseErrorMessage();
		}

        currentIndex--;
        if (currentIndex < 0)
        {
            currentIndex = SkinsController.skController.allSkinTextures.Length - 1;
        }

		playerRenderer.sprite = SkinsController.skController.allSkinTextures[currentIndex];
        //playerRenderer.material.mainTexture = SkinsController.skController.allSkinTextures[currentIndex];
    }

    private void ActivateCorrectSkinButton(int index)
    {
        if (index == 0)
        {
            unlockSkinButton.SetActive(true);
            useSkinButton.SetActive(false);
        }
        else if (index == 1)
        {
            unlockSkinButton.SetActive(false);
            useSkinButton.SetActive(true);
        }
        else
        {
            unlockSkinButton.SetActive(false);
            useSkinButton.SetActive(false);
        }
    }
}
