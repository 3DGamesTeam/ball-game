﻿using UnityEngine;
using System.Collections;

public class PlatformScript : MonoBehaviour
{
    public PlayerController player;
    public Collider2D col;


    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        col = GetComponent<Collider2D>();
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.isGhost == true)
        {
            col.enabled = false;
        }
        else
        {
            col.enabled = true;
        }
    }






}
