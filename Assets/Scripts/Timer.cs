﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	public float timer;
	public Text timerHolder;

	// Use this for initialization
	void Start () {
		timerHolder = GameObject.Find ("Timer").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		timer += Time.deltaTime;
		timerHolder.text = "Time: " + timer.ToString("F2");
	}
}
