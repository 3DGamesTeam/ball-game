﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TokenSystem : MonoBehaviour
{
    public Text tokenText;

    // Update is called once per frame
    void Update()
    {
        if (GameController.gController.tokens <= 0)
        {
            GameController.gController.tokens = 0;
        }

        // Made in order to avoid NullReferenceError in Level scenes;
        if (tokenText != null)
        {
            tokenText.text = GameController.gController.tokens.ToString();
        }
    }


    // Methods for skins/shop/ads
    public void addTokens(int amount)
    {
        GameController.gController.tokens += amount;
    }

    public void removeTokens(int sum)
    {
        // Check if you have enough tokens.
        if (GameController.gController.tokens < sum)
        {
            Debug.Log("Cannot buy stuff or unlock the next level.");
            return;
        }

        GameController.gController.tokens -= sum;
    }
}
