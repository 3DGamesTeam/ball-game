﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpenLevels : MonoBehaviour
{
    private int i;
    public Texture activeStar;
    public GameObject lockTexture;
    private RawImage Star1;
    private RawImage Star2;
    private RawImage Star3;

    // Use this for initialization
    void Start()
	{
		i = 1;

		for (int levelIndex = 0; levelIndex < GameController.gController.playerData.Count; levelIndex++) {
			// Check Key for Every level and put stars
			if (GameController.gController.playerData [levelIndex].Stars > 0) {
				Star1 = GameObject.Find ("Level" + i + "/Star1").GetComponent<RawImage> ();
				Star2 = GameObject.Find ("Level" + i + "/Star2").GetComponent<RawImage> ();
				Star3 = GameObject.Find ("Level" + i + "/Star3").GetComponent<RawImage> ();

				if (GameController.gController.playerData [levelIndex].Stars >= 1) {
					Star1.texture = activeStar;
				}

				if (GameController.gController.playerData [levelIndex].Stars >= 2) {
					Star2.texture = activeStar;
				}

				if (GameController.gController.playerData [levelIndex].Stars == 3) {
					Star3.texture = activeStar;
				}
			}

			// Unlock Level which exists in the collection playerData in GameController
			// This is where we keep information for each specific level
			if (i > 1) {
				lockTexture = GameObject.Find ("Level" + i + "/Lock");
				lockTexture.SetActive (false);
				GameObject.Find ("Level" + i).GetComponent<Button> ().enabled = true;
			}

			i++;
		}
    }
}
