﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameLevelTimes
{
    public static Dictionary<string, float> levelTimes = new Dictionary<string, float>()
    {
        { "Level1", 20.0f },
        { "Level2", 20.0f },
        { "Level3", 20.0f },
        { "Level4", 20.0f },
        { "Level5", 20.0f },
        { "Level6", 20.0f },
        { "Level7", 20.0f },
        { "Level8", 20.0f },
        { "Level9", 20.0f },
        { "Level10", 20.0f },
        { "Level11", 20.0f },
        { "Level12", 20.0f },
        { "Level13", 20.0f },
        { "Level14", 20.0f },
        { "Level15", 20.0f },
        { "Level16", 20.0f },
		{ "Level17", 20.0f },
		{ "Level18", 20.0f },
		{ "Level19", 20.0f },
		{ "Level20", 20.0f },
		{ "Level21", 20.0f },
		{ "Level22", 20.0f },
		{ "Level23", 20.0f },
		{ "Level24", 20.0f }
    };

	public const int UnlockLevelWithTokensPrice = 1000;
	public const int GhostPowerUpPrice = 500;
	public const int SpeedPowerUpPrice = 250;
	public const int WatchVideoAdReward = 50;
	public const int UnlockNextLevelPrice = 10;
}