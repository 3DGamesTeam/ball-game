﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class FinishLine : MonoBehaviour
{
    public float currentTime;
    private GameObject timer;
    public string sceneName;
    private RawImage star1;
    private RawImage star2;
    private RawImage star3;
    private GameObject lockLevel;
    private GameObject nextScene;
    private GameObject finalScore;
    public GameObject unlockLevelMenu;
    public Texture activeStar;
    public GameObject levelScore;
    public int currentSceneIndex;

    private GameObject buttonPressed;
    public Button ghostButton;
    public Button speedButton;
    public GameObject infoMessage;
    public Text infoText;

    void Start()
    {
        // Get current scene name 
        sceneName = SceneManager.GetActiveScene().name;

        // -2 is used to covert the game build index to array index starting from 0, because the first level scene's build index is 2.
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex - 2;

        // Asign Stars to variables
        star1 = GameObject.Find("1").GetComponent<RawImage>();
        star2 = GameObject.Find("2").GetComponent<RawImage>();
        star3 = GameObject.Find("3").GetComponent<RawImage>();

        // Asign level lock to variable
        lockLevel = GameObject.Find("LockLevel");

        // Asign Next Level btn to variable
        nextScene = GameObject.Find("Next");

        // When player hit finish line we get current time and assign it on Your score is:
        finalScore = GameObject.Find("YourTime");

        // Asign Camera to canera variable
        //  camera = GameObject.Find("Camera");

        // On start we hide levelscore GameObject and asign it on a varible levelScore
        levelScore = GameObject.Find("LevelScore");
        levelScore.SetActive(false);

        // Asign Unlock level menu on variable
        unlockLevelMenu = GameObject.Find("UnlockLevel");
        unlockLevelMenu.SetActive(false);

        // Get the information message game object
        infoMessage = GameObject.Find("InfoMessage");
        infoMessage.SetActive(false);

        // Assign powerup buttons to disable them, when we get to the finish line.
        ghostButton = GameObject.Find("GhostButton").GetComponent<Button>();
        speedButton = GameObject.Find("SpeedButton").GetComponent<Button>();

        if (GameController.gController.playerData[currentSceneIndex].BestTime > 0)
        {
            GameObject.Find("BestTime").GetComponent<Text>().text = "Best Time: " + GameController.gController.playerData[currentSceneIndex].BestTime.ToString("F2");
        }
        
    }

    void Update()
    {
        if (unlockLevelMenu.activeSelf)
        {
            buttonPressed = EventSystem.current.currentSelectedGameObject;
            if (buttonPressed == null || (!buttonPressed.name.Equals("LockLevel") && !buttonPressed.name.Equals("Unlock")))
            {
                unlockLevelMenu.SetActive(false);
            }
        }

        if (infoMessage.activeSelf)
        {
            buttonPressed = EventSystem.current.currentSelectedGameObject;
            if (buttonPressed != null && (buttonPressed.name.Equals("Close") || buttonPressed.name.Equals("AdButton")))
            {
                infoMessage.SetActive(false);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        // Check if collision target have tag Player
        if (target.gameObject.tag == "Player")
        {

            target.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            currentTime = GameObject.Find("Timer").GetComponent<Timer>().timer;

            // Stop Timer
            stopTimer();

            if (GameController.gController.playerData[currentSceneIndex].BestTime <= 0)
            {
                GameController.gController.playerData[currentSceneIndex].BestTime = currentTime;
                GameObject.Find("BestTime").GetComponent<Text>().text = "Best Time: " + GameController.gController.playerData[currentSceneIndex].BestTime.ToString("F2");
            }

            if (currentTime < GameController.gController.playerData[currentSceneIndex].BestTime)
            {
                GameController.gController.playerData[currentSceneIndex].BestTime = currentTime;
                GameObject.Find("BestTime").GetComponent<Text>().text = "Best Time: " + GameController.gController.playerData[currentSceneIndex].BestTime.ToString("F2");
            }

            // Show Level Stars and Action Btns
            levelScoreUI();

            ghostButton.enabled = false;
            speedButton.enabled = false;
        }
    }

    void stopTimer()
    {
        timer = GameObject.Find("Timer");
        timer.GetComponent<Timer>().enabled = false;
    }

    void levelScoreUI()
    {
        levelScore.SetActive(true);
        finalScore.GetComponent<Text>().text = "Your Time is: " + currentTime.ToString("F2");

        CheckLevelScore(sceneName, currentSceneIndex, currentTime);
    }

    public void unlockNextLevel(string level)
    {
        // Remove Lock image and active script 
        lockLevel.SetActive(false);
        nextScene.GetComponent<Button>().enabled = true;
    }

    private void CheckLevelScore(string levelName, int levelIndex, float currentLevelTime)
    {
        //I use levelIndex + 2 to determine the number of the next level,
        //because levelIndex is the index of the current level in PlayerData in GameController (Level1 -> 0, Level4 -> 3, etc.)
        //So, if we are now playing Level7 with levelIndex 6
        //and in order to compose the next level name Level8 -> it would be "Level" + levelIndex(6) + 2 applied as string.
        //It is actually the same as doing - "Level" + SceneManager.GetActiveScene().buildIndex, but this may cause problems in the future.
        string nextLevelName = "Level" + (levelIndex + 2);
        //Debug.Log("Next level should be....." + nextLevelName);

        float levelTime = GameLevelTimes.levelTimes[levelName];

        if (currentLevelTime <= levelTime + 10.0f)
        {
            star1.texture = activeStar;

            if (GameController.gController.playerData[levelIndex].Stars < 1)
            {
                GameController.gController.playerData[levelIndex].Stars = 1;
                RewardTokensForLevelUnlock(levelIndex + 1);

                if (GameController.gController.playerData.Count == levelIndex + 1)
                {
                    GameController.gController.playerData.Add(new LevelData(nextLevelName, 0, 0.0f));
                }
            }
        }

        if (currentLevelTime <= levelTime + 5.0f)
        {
            star2.texture = activeStar;

            if (GameController.gController.playerData[levelIndex].Stars < 2)
            {
                GameController.gController.playerData[levelIndex].Stars = 2;
            }
        }

        if (currentLevelTime <= levelTime)
        {
            star3.texture = activeStar;

            if (GameController.gController.playerData[levelIndex].Stars < 3)
            {
                GameController.gController.playerData[levelIndex].Stars = 3;
            }
        }

        //Here I check if we have already unlocked more levels than the level we are currently playing.
        //Example, we are playing Level7 - it has levelIndex 6. PlayerData.Count will be 7 (levelIndex 6 + 1).
        //In order to check if Level8 is aready unlocked (it has levelIndex 7), PlayerData.Count here will be 8 (levelIndex 7 + 1)
        //we need to check if PlayerData.Count is > 7 => levelIndex + 1
        if (GameController.gController.playerData.Count > levelIndex + 1)
        {
            unlockNextLevel(nextLevelName);
        }
    }

    private void RewardTokensForLevelUnlock(int level)
    {
        ShowInfoMessage(level * GameLevelTimes.UnlockNextLevelPrice);
        GameController.gController.tokens += (level * GameLevelTimes.UnlockNextLevelPrice);
    }

    private void ShowInfoMessage(int tokens)
    {
        infoMessage.SetActive(true);
        infoText = infoMessage.GetComponentInChildren<Text>();
        infoText.text = string.Format("You have been rewarded {0} tokens for reaching at least 1 star on this level!", tokens);
    }
}
