﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundController : MonoBehaviour
{
	public static SoundController soundInstance;

	public AudioSource audio1;
	public AudioSource audio2;

	void Awake()
	{
		if (soundInstance == null)
		{
			soundInstance = this;
			soundInstance.PlayMusic();
		}
		else if(soundInstance != this)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	public void PlayMusic()
	{
		soundInstance.audio1.Play();
	}

	public void CheckMusic()
	{
		//Audio1 => Music for all scenes different than the specific ball levels.
		//Audio2 => Music for the different ball levels.
		string levelName = SceneManager.GetActiveScene().name;
		string levelNamePrefix = levelName.Substring(0, 5);

		if (levelNamePrefix.Equals("Level") && !levelName.Equals("Levels_Menu") && soundInstance.audio1.isPlaying) 
		{
			soundInstance.audio1.Stop();
			soundInstance.audio2.Play();
		}
		else if (!levelNamePrefix.Equals("Level") || levelName.Equals("Levels_Menu") && soundInstance.audio2.isPlaying)
		{
			soundInstance.audio1.Play();
			soundInstance.audio2.Stop();
		}
	}

	public void ControlMute(bool shouldMute)
	{
		soundInstance.audio1.mute = shouldMute;
		soundInstance.audio2.mute = shouldMute;
	}
}
