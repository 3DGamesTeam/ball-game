﻿using UnityEngine;
using System.Collections;

public class share : MonoBehaviour
{

    public string AppID;
    public string Link;
    public string Picture;
    public string Name;
    public string Caption;
    public string Description;
    private string currentLevel;
    private float currentTime;

    void playerTime() {
        // Get Current scene name and player current time
        currentLevel = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().sceneName;
        currentTime = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinishLine>().currentTime;

        // Game Share info
        Name = "Time Ball";
        AppID = "1695988283985823";
        Picture = "http://images.clipartpanda.com/smiley-face-clip-art-thumbs-up-large-Smiley-Face-making-Thumbs-Up-0-16636.png";
        Link = "https://facebook.com/";
        Description = "Enjoy this free game! Challenge yourself and your friends.";
        Caption = "My new Score on " + currentLevel + " is " + currentTime.ToString("F2") + "sec. Can u beat it?";
        Debug.Log(Caption);

    }

    public void FacebookShare()
    {
        playerTime();

        Application.OpenURL("https://www.facebook.com/dialog/feed?" +
                "app_id=" + AppID +
                "&link=" + Link +
                "&picture=" + Picture +
                "&name=" + SpaceHere(Name) +
                "&caption=" + SpaceHere(Caption) +
                "&description=" + SpaceHere(Description) +
                "&redirect_uri=https://facebook.com/");
        // Remove Share btn
        Destroy(this.gameObject);
    }
    string SpaceHere(string val)
    {
        return val.Replace(" ", "%20"); // %20 is only used for space
    }
}