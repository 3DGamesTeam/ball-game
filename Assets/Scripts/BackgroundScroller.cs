﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour
{

    public float speed;
    public MeshRenderer rend;
    float pos = 0;

    // 1 is up , -1 is down
    public void BackgroundScroll(float directionIndex)
    {
        if (directionIndex == 1)
        {
            pos += speed;
        }
        else if (directionIndex == -1)
        {
            pos -= speed;
        }
        rend.material.mainTextureOffset = new Vector2(0, pos);

    }
}
