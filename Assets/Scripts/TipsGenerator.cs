﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TipsGenerator : MonoBehaviour {

    private string text1 = "When you share your level time on facebook you get 50 tokens.";
    private string text2 = "If you watch ad video you will get 150 tokens.";
    private string text3 = "When you finish level you get tokens.";
    private string text4 = "With tokens you can buy skins for your ball and unlock levels.";
    private Text text;
    private int random;

	// Use this for initialization
	void Start () {
        text = GameObject.Find("Tips").GetComponent<Text>();
        random = Random.Range(1, 10);

        if (random == 1)
        {
            text.text = "TIPS: " + text1;
        }

        if (random == 2)
        {
            text.text = "TIPS: " + text2;
        }

        if (random == 3)
        {
            text.text = "TIPS: " + text3;
        }

        if (random == 4)
        {
            text.text = "TIPS: " + text4;
        }

	
	}
}
