﻿using UnityEngine;
using System.Collections;

public class autoRotate : MonoBehaviour {

    public float rotateSpeed = 100;
	
	// Update is called once per frame
	void FixedUpdate () {

        this.transform.eulerAngles += new Vector3(0.0f, 0.0f, Time.deltaTime * rotateSpeed);
	
	}

    

}
