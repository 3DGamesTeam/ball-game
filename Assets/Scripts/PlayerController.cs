﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float ghostTimer;
    public float boostTimer;
    public float directionIndex = -1.0f;
    public bool isGhost;
    public bool isGrounded;
    public bool speedBoost;
    private Rigidbody2D rb;
    public BackgroundScroller background;
    public SphereCollider[] sphereCol;
    public Text ghostPowersText;
    public Text speedPowerText;
    public SpriteRenderer rend;
    public TrailRenderer trail;
    private float moveHorizontal;

    void Awake()
    {
        if (SkinsController.skController.skinShopData[GameController.gController.lastUsedSkinIndex].IsBought)
        {
            this.GetComponent<SpriteRenderer>().sprite = SkinsController.skController.allSkinTextures[GameController.gController.lastUsedSkinIndex];
            //this.GetComponent<Renderer>().material.mainTexture = SkinsController.skController.allSkinTextures[GameController.gController.lastUsedSkinIndex];
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = SkinsController.skController.allSkinTextures[0];
            //this.GetComponent<Renderer>().material.mainTexture = SkinsController.skController.allSkinTextures[0];
        }
    }

    void Start()
    {
        speedPowerText = GameObject.Find("SpeedButton").GetComponentInChildren<Text>();
        ghostPowersText = GameObject.Find("GhostButton").GetComponentInChildren<Text>();
        sphereCol = GetComponents<SphereCollider>();
        rb = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();
        background = FindObjectOfType<BackgroundScroller>().GetComponent<BackgroundScroller>();
        trail = GetComponent<TrailRenderer>();
        trail.enabled = false;
        speedBoost = false;
        isGhost = false;
    }

    void FixedUpdate()
    {
#if UNITY_STANDALONE_WIN
        moveHorizontal = Input.GetAxis("Horizontal");
        if (Input.GetAxis("Vertical") > 0 && Physics2D.gravity.y < 0)
        {
            InvertGravity();
        }
        else if (Input.GetAxis("Vertical") < 0 && Physics2D.gravity.y > 0)
        {
            InvertGravity();
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            moveHorizontal = 1;
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            moveHorizontal = -1;
        }
#endif

#if UNITY_ANDROID

        moveHorizontal = Input.acceleration.x;

        if (Input.acceleration.y > 0 && Physics2D.gravity.y < 0)
        {
            InvertGravity();
        }
        else if (Input.acceleration.y < 0 && Physics2D.gravity.y > 0)
        {
            InvertGravity();
        }
        if (Input.acceleration.x > 0)
        {
            moveHorizontal = 1;
        }
        if (Input.acceleration.x < 0)
        {
            moveHorizontal = -1;
        }





#endif

#if UNITY_STANDALONE_OSX
		moveHorizontal = Input.GetAxis("Horizontal");
		if (Input.GetAxis("Vertical") > 0 && Physics2D.gravity.y < 0)
		{
			InvertGravity();
		}
		else if (Input.GetAxis("Vertical") < 0 && Physics2D.gravity.y > 0)
		{
			InvertGravity();
		}
        if (Input.GetAxis("Horizontal")> 0)
        {
            moveHorizontal = 1;
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            moveHorizontal = -1;
        }
#endif


        //rb.velocity = new Vector2((moveHorizontal * speed) , 0) ;   

        Vector2 movement = new Vector2(moveHorizontal * speed, 0.0f);
        rb.AddForce(movement);
        if (!isGrounded)
        {
            background.BackgroundScroll(directionIndex);
        }
       
        speedPowerText.text = GameController.gController.speedPowerUps.ToString();
        ghostPowersText.text = GameController.gController.ghostPowerUps.ToString();
        // GHOST MODE CALCULATIONS
        if (isGhost)
        {
            // Enables the collider after period of time;
            rend.material.color = new Color(1f, 1f, 1f, .5f);
            ghostTimer += Time.deltaTime;
            if (ghostTimer > 0.75f)
            {
                rend.material.color = Color.white;
                isGhost = false;
                ghostTimer = 0;
            }
        }

        if (speedBoost)
        {
            boostTimer += Time.deltaTime;
            trail.enabled = true;
            speed = 80f;
            if (boostTimer > 1.0f)
            {
                speed = 12f;
                speedBoost = false;
                trail.enabled = false;
                boostTimer = 0;
            }
        }

    }

    public void increaseSpeed()
    {
        if (GameController.gController.speedPowerUps > 0 && !speedBoost)
        {
            speedBoost = true;
            Debug.Log(speedBoost);

            GameController.gController.speedPowerUps -= 1;
        }
    }

    public void ghostMode()
    {

        if (GameController.gController.ghostPowerUps > 0 && isGhost == false)
        {
            isGhost = true;
            GameController.gController.ghostPowerUps -= 1;
            Debug.Log("TOKEN TAKEN");
        }
    }






    private void InvertGravity()
    {
        Physics2D.gravity = -Physics2D.gravity;
        directionIndex = -directionIndex;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.gameObject.tag != "Wall" || col.transform.gameObject.name == "TopCollider")
        {
            isGrounded = true;
        }
        
    }
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.transform.gameObject.tag != "Wall" || col.transform.gameObject.name == "TopCollider")
        {
            isGrounded = true;
        }
        
    }
    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag != "Wall" || col.transform.name == "TopCollider")
        {
            isGrounded = false;
        }
        if(col.gameObject.tag == "Finish")
        {
            isGrounded = true;
        }
    }
}

